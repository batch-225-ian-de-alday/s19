// console.log("Hellow Ian")



// Conditional Statements - allows us to control the flow of our program

// [SECTION] if, else if, else Statement

/*
Syntax:
	
	if(condition){
		statement
	}

*/

// if Statement

let numA = 5;

if(numA > 3) {
	console.log("Hello");
}


let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}


//  else if Clause


/*
	- Executes a statement if previous condition are false adn if the specified condition is true
	- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

// else if() statement was no longer run because the if statement was able to run and the evaluation is passed

let numB = 1;

if(numA < 3){ // numA = 5 (in the top) // false
	console.log("Hellow");
} else if(numB > 0) {
	console.log("World"); //displayed
}

// ===========

if(numA > 3){ // numA = 5 (in the top) // false
	console.log("Hellow"); //displayed
} else if(numB > 0) {
	console.log("World");
}


// else if in string

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York city!");
} else if(city === "Tokyo") {
	console.log("Welcome to Tokyo, Japan!");
}

// else Statement
/*
	- Executes a statement if all other conditions are false
	- The 'else' statement is optional and can be added to capture any other result to change the flow of a program.
	
*/



let numC = -5;
let numD = 7;

if(numC > 0) { //false
	console.log('Hello');
} else if (numD === 0) { //false
	console.log('World');
} else {
	console.log('Error');
}

// if, else if and else statements with function

function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return "Not a typhoon yet.";
	
	}
	else if (windSpeed <= 61) {
		return "Tropical depression deteceted.";
	}
	else if (windSpeed >= 62 && windSpeed <= 88) {
		// && means AND operator to make sure that 62-88 is  detected accurately
		return "Tropical storm detected.";
	}
	else if (windSpeed >= 89 && windSpeed <= 117) {
		//  && is used to make sure 89-117 will be detected accurately
		return "Severe Tropical strom detected.";
	}
	else {
		return "Typhoon detected.";
	}
}
let message = determineTyphoonIntensity(87);
console.log(message);

if (message == "Tropical storm detected.") {
	console.warn(message);
} //will display warnin sign. This is used for detecting error


// [SECTION] Conditional (Ternary) Operator
/*
	- The Conditional (Ternary) Operator takes three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy

	// Ternary operator is for shorthand code - commonly used for single statement execution where the result consists if only one line of code

	- Syntax
		(expression) ? ifTrue : ifFalse

		- can be used as an alternative to an "if else" statement

*/
let t = "yes";
let f = "no";

// Single statement execution
let	ternaryResult = (1 < 18) ? t : f
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement execution

let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// the "parseInt" function converts the input recieve into a number data type.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);


// [SECTION] Switch Statement

/*
	- the switch statements evaluates an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	- can be used an alternative to an "if", "else if and else" statement where the data to be used in the condition in an expected output
	- The ".toLowerCase()" function/method will change input received from the prompt into all lowercase letters ensuring a match with the switch case conditions of the user inputs capitalized or uppercase letters.
*/

/*
	switch (expression) {
		case value:
			statement;
			break;
			default: statement; 
	}

*/

let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;
}